//
//  Constants.h
//  Test
//
//  Created by Ion Silviu-Mihai on 07/02/16.
//  Copyright © 2016 Ion Silviu-Mihai. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kGetContactsURL @"http://downloadapp.youwow.me/iPhone/iOSTest/contacts.json"

#define kJSONKeyFirstName @"firstName"
#define kJSONKeyLasttName @"lastName"
#define kJSONKeyStatusIcon @"statusIcon"
#define kJSONKeyStatusMessage @"statusMessage"