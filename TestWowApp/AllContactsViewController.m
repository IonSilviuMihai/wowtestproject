//
//  AllContactsViewController.m
//  Test
//
//  Created by Ion Silviu-Mihai on 06/02/16.
//  Copyright © 2016 Ion Silviu-Mihai. All rights reserved.
//

#import "AllContactsViewController.h"
#import "AppModel.h"
#import "Contact.h"
#import "MBProgressHUD.h"

@interface AllContactsViewController () <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

@property (strong, nonatomic) NSMutableArray *groupNamesArray;
@property (strong, nonatomic) NSMutableArray *peopleArray;

@property (strong, nonatomic) NSMutableDictionary *dataSourceDictionary;

@property (strong	, nonatomic) NSArray *tableViewElements;
@property (strong	, nonatomic) NSArray *tableViewSections;

@property (strong, nonatomic) UIRefreshControl *refreshControl;

@end

@implementation AllContactsViewController

#pragma mark - Setters and Getters

- (NSMutableDictionary *)dataSourceDictionary
{
    if (_dataSourceDictionary == nil)
    {
        _dataSourceDictionary = [[NSMutableDictionary alloc] init];
    }
    return _dataSourceDictionary;
}

- (NSMutableArray *)groupNamesArray {
    return [AppModel sharedModel].groupNamesArray;
}

- (NSMutableArray *)peopleArray {
    return [AppModel sharedModel].peopleArray;
}

#pragma mark - Lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];

    self.tableView.delegate = self;
    self.tableView.dataSource = self;

    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.tintColor = [UIColor lightGrayColor];
    [self.tableView addSubview: self.refreshControl];
    [self.refreshControl addTarget:self action:@selector(getData) forControlEvents:UIControlEventValueChanged];
    
    self.searchBar.delegate = self;
    self.searchBar.showsCancelButton = YES;
    
    if (!self.peopleArray) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [self getData];
    }
    else {
        [self loadItems];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - General Methods
- (void)getData {
    [[AppModel sharedModel] getPeopleFromServerWithSucces:^(bool success) {
        [self loadItems];
    } error:^(NSError *error) {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"Error"
                                              message:@"Contacts download failed. Please try again later."
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"OK action");
                                   }];
        
        [alertController addAction:okAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }];
}

- (void)loadItems {
    dispatch_queue_t queue = dispatch_queue_create("Create DataSource", NULL);
    dispatch_async(queue, ^{
        
        NSMutableArray *sectionBuilder = [[NSMutableArray alloc] init];
        NSMutableArray *sectionHeaders = [[NSMutableArray alloc] init];
        
        for (int i = 0 ; i < self.groupNamesArray.count ; i++)
        {
            NSString *groupName = self.groupNamesArray[i];
            
            NSArray *peopleArray;
            if (self.searchBar.text.length > 0) {
                NSString *searchString = self.searchBar.text;
                peopleArray = [self.peopleArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"%K == %@ AND (%K contains[c] %@ OR %K contains[c] %@)", @"groupName", groupName, @"firstName", searchString, @"lastName", searchString]];
            }
            else {
                peopleArray = [self.peopleArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"%K == %@", @"groupName", groupName]];
            }

            if (peopleArray.count > 0)
            {
                [sectionBuilder addObject:peopleArray];
                [sectionHeaders addObject:groupName];
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            [self.refreshControl endRefreshing];
            
            self.tableViewElements = [sectionBuilder copy];
            self.tableViewSections = [sectionHeaders copy];
            
            [self.tableView reloadData];
        });
    });
}

#pragma mark - IBActions
- (IBAction)groupsBarButtonPressed:(UIBarButtonItem *)sender {
}

- (IBAction)addBarButtonPressed:(UIBarButtonItem *)sender {
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.tableViewSections.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.tableViewElements[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Contact *currentContact = self.tableViewElements[indexPath.section][indexPath.row];

    static NSString *identifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    
    UILabel *nameLabel = (UILabel *)[cell.contentView viewWithTag:1];
    UILabel *messageLabel = (UILabel *)[cell.contentView viewWithTag:2];
    UIImageView *statusImageView = (UIImageView *)[cell.contentView viewWithTag:3];
    UIImageView *profilePictureImageView = (UIImageView *)[cell.contentView viewWithTag:4];
    
    nameLabel.text = @"";
    messageLabel.text = @"";
    statusImageView.image = nil;
    profilePictureImageView.image = nil;
    
    if (currentContact.firstName.length > 0 && currentContact.lastName.length > 0) {
        nameLabel.text = [NSString stringWithFormat:@"%@ %@", currentContact.firstName, currentContact.lastName];
    }
    else if (currentContact.firstName.length > 0) {
        nameLabel.text = currentContact.firstName;
    }
    else if (currentContact.lastName.length > 0) {
        nameLabel.text = currentContact.lastName;
    }
    
    messageLabel.text = currentContact.statusMessage.length > 0 ? currentContact.statusMessage : [currentContact.statusIcon capitalizedString];

    statusImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"contacts_list_status_%@", currentContact.statusIcon]];
    
    //server data does not have any informations about profile picture or contact's sex
    profilePictureImageView.image = [UIImage imageNamed:@"contacts_list_avatar_unknown"];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"Did tap on section: %ld, row: %ld", (long)indexPath.section, (long)indexPath.row);
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 22;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 22)];
    headerView.backgroundColor = [UIColor lightGrayColor];
    
    UILabel *groupNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(8, 0, tableView.frame.size.width - 8 * 2, 22)];
    groupNameLabel.textColor = [UIColor whiteColor];
    groupNameLabel.text = [self.tableViewSections[section] capitalizedString];
    
    [headerView addSubview:groupNameLabel];
    
    return headerView;
}

#pragma mark - UISearchBar Delegate
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    return YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [self loadItems];
}
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    searchBar.text = @"";
    [self loadItems];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

#pragma mark - UIScrollViewDelegate Implementation
- (void) scrollViewWillBeginDragging:(UIScrollView*)scrollView {
    [self.searchBar resignFirstResponder];
}

@end
