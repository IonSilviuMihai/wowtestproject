//
//  AppDelegate.h
//  TestWowApp
//
//  Created by Ion Silviu-Mihai on 07/02/16.
//  Copyright © 2016 Ion Silviu-Mihai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

