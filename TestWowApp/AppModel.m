//
//  AppModel.m
//  Peekus
//
//  Created by Silviu-Mihai Ion on 11/4/14.
//  Copyright (c) 2014 Geronimo. All rights reserved.
//

#import "AppModel.h"
#import "Contact.h"
#import "Constants.h"

@implementation AppModel

+ (AppModel *)sharedModel
{
    static AppModel *sharedModel;
    if (sharedModel == nil)
    {
        sharedModel = [[AppModel alloc] init];
    }
    return sharedModel;
}

#pragma mark - Setters
- (NSMutableArray *)groupNamesArray
{
    if (_groupNamesArray == nil)
    {
        _groupNamesArray = [[NSMutableArray alloc] init];
    }
    return _groupNamesArray;
}

#pragma mark - Notifications Strings
-(void)getPeopleFromServerWithSucces:(void (^)(bool)) succesHandler
                               error:(void (^)(NSError *)) errorHandler {
    NSURL *url = [NSURL URLWithString:kGetContactsURL];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    [[session dataTaskWithRequest:request
                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                    if (!error) {
                        NSError *serializeError = nil;
                        NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&serializeError];
                        NSInteger success = 1;
                        success = [jsonData[@"ERROR"] integerValue];
                        if (success == 0) {
                            self.peopleArray = [[NSMutableArray alloc] init];
                            NSMutableArray *groupsArray = jsonData[@"groups"];
                            for (NSDictionary *groupDict in groupsArray) {
                                if (![self.groupNamesArray containsObject:groupDict[@"groupName"]]) {
                                    [self.groupNamesArray addObject:groupDict[@"groupName"]];
                                }
                                NSMutableArray *peopleArray = groupDict[@"people"];
                                for (NSDictionary *personDict in peopleArray) {
                                    Contact *newContact = [[Contact alloc] initWithDictionary:personDict andGroupName:groupDict[@"groupName"]];
                                    [self.peopleArray addObject:newContact];
                                }
                            }
                            
                            self.groupNamesArray = [[self.groupNamesArray sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] mutableCopy];
                            
                            succesHandler(true);
                        }
                        else {
                            errorHandler(nil);
                        }
                    }
                    else {
                        errorHandler(error);
                    }
                }] resume];
}

@end
