//
//  Person.h
//  Test
//
//  Created by Ion Silviu-Mihai on 07/02/16.
//  Copyright © 2016 Ion Silviu-Mihai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject

@property (nonatomic) NSString *firstName;
@property (nonatomic) NSString *lastName;

@end
