//
//  AppModel.h
//  Test
//
//  Created by Ion Silviu-Mihai on 06/02/16.
//  Copyright © 2016 Ion Silviu-Mihai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppModel : NSObject

@property (strong, nonatomic) NSMutableArray *groupNamesArray;
@property (strong, nonatomic) NSMutableArray *peopleArray;

+ (AppModel *)sharedModel;

//server communication
-(void)getPeopleFromServerWithSucces:(void (^)(bool)) succesHandler
                               error:(void (^)(NSError *)) errorHandler;

@end
