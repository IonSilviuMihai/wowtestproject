//
//  Contact.h
//  Test
//
//  Created by Ion Silviu-Mihai on 07/02/16.
//  Copyright © 2016 Ion Silviu-Mihai. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Person.h"

@interface Contact : Person

@property (nonatomic) NSString *statusIcon;
@property (nonatomic) NSString *statusMessage;
@property (nonatomic) NSString *groupName;

- (instancetype)initWithDictionary:(NSDictionary *)dict andGroupName:(NSString *)groupName;

@end
