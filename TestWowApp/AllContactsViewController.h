//
//  AllContactsViewController.h
//  Test
//
//  Created by Ion Silviu-Mihai on 06/02/16.
//  Copyright © 2016 Ion Silviu-Mihai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AllContactsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)groupsBarButtonPressed:(UIBarButtonItem *)sender;
- (IBAction)addBarButtonPressed:(UIBarButtonItem *)sender;

@end
