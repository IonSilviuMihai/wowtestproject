//
//  Contact.m
//  Test
//
//  Created by Ion Silviu-Mihai on 07/02/16.
//  Copyright © 2016 Ion Silviu-Mihai. All rights reserved.
//

#import "Contact.h"

#import "Constants.h"

@implementation Contact
- (instancetype)initWithDictionary:(NSDictionary *)dict andGroupName:(NSString *)groupName {
    self = [super init];
    if (self) {
        self.firstName = dict[kJSONKeyFirstName];
        self.lastName = dict[kJSONKeyLasttName];
        self.statusIcon = dict[kJSONKeyStatusIcon];
        self.statusMessage = dict[kJSONKeyStatusMessage];
        self.groupName = groupName;
    }
    return self;
}

@end
